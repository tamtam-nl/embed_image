<?php


/**
 * @file
 * Contains \Drupal\embed_image\Twig\EmbedImageExtension.
 */

namespace Drupal\embed_image\Twig;

use Drupal\file\Entity\File;

/**
 * Class EmbedViewExtension
 * Print a menu
 * @package Drupal\embed_view\Twig
 */
class EmbedImageExtension extends \Twig_Extension
{
    protected $imageFactory;
    protected $renderer;
    protected $imageStyleStorage;


    /**
     * EmbedImageExtension constructor.
     */
    public function __construct()
    {
        $this->imageFactory = \Drupal::service('image.factory');
        $this->renderer = \Drupal::service('renderer');
        $this->imageStyleStorage = \Drupal::entityTypeManager()->getStorage('image_style');
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'embed_image';
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('embed_responsive_image', [
                $this,
                'embedRepsonsiveImage'
            ], [
                'is_safe' => ['html'],
            ]),
            new \Twig_SimpleFunction('embed_image', [$this, 'embedImage'], [
                'is_safe' => ['html'],
            ]),
            new \Twig_SimpleFunction('img_style_url', [
                $this,
                'getImageStyleUrl'
            ], [
                'is_safe' => ['html'],
            ]),
            new \Twig_SimpleFunction('get_image_component_data', [
                $this,
                'getImageComponentData'
            ], [
                'is_safe' => ['html'],
            ]),
        ];
    }


    /**
     * Embed Responsive image
     *
     * @param $target_id
     * @param string $responsive_image_style_id
     * @return array
     */
    public function embedRepsonsiveImage(
        $target_id,
        $responsive_image_style_id = ''
    ) {
        $file = File::load($target_id);

        if ($file) {
            $variables = [
                'responsive_image_style_id' => $responsive_image_style_id,
                'uri'                       => $file->getFileUri(),
            ];

            $image = $this->imageFactory->get($file->getFileUri());

            if ($image->isValid()) {
                $variables['width'] = $image->getWidth();
                $variables['height'] = $image->getHeight();
            } else {
                $variables['width'] = $variables['height'] = null;
            }

            template_preprocess_responsive_image($variables);
            template_preprocess_image($variables);

            $responsiveImage = [
                '#theme'                     => 'responsive_image',
                '#width'                     => $variables['width'],
                '#height'                    => $variables['height'],
                '#responsive_image_style_id' => $variables['responsive_image_style_id'],
                '#uri'                       => $variables['uri'],
                '#attributes'                => $variables["attributes"],
            ];


            $this->renderer->addCacheableDependency($responsiveImage, $file);

            return [$responsiveImage];
        }

        return [];
    }

    /**
     * @param $target_id
     * @param string $image_style
     * @return array
     */
    public function embedImage($target_id, $image_style = 'thumbnail')
    {
        $file = File::load($target_id);

        if ($file) {
            $variables = array(
                'style_name' => $image_style,
                'uri'        => $file->getFileUri(),
            );

            $image = $this->imageFactory->get($file->getFileUri());

            if ($image->isValid()) {
                $variables['width'] = $image->getWidth();
                $variables['height'] = $image->getHeight();
            } else {
                $variables['width'] = $variables['height'] = null;
            }

            $imageBuild = [
                '#theme'      => 'image_style',
                '#width'      => $variables['width'],
                '#height'     => $variables['height'],
                '#style_name' => $variables['style_name'],
                '#uri'        => $variables['uri'],
            ];

            $this->renderer->addCacheableDependency($imageBuild, $file);

            return [$imageBuild];
        }

        return [];
    }

    /**
     * @param $target_id
     * @param string $image_style
     * @return array
     */
    public function getImageStyleUrl($imageUri, $image_style = 'thumbnail')
    {
        $url = null;

        if (empty($imageUri) === false) {
            $url = $this->imageStyleStorage->load($image_style)->buildUrl($imageUri);
        }

        return $url;
    }

    /**
     * Return data for frontend image component
     *
     * @param $target_id
     * @param string $responsive_image_style_id
     * @param string $preload_image_style
     * @return array
     */
    public function getImageComponentData(
        $target_id,
        $responsive_image_style_id = '',
        $preload_image_style = 'thumbnail'
    ) {
        $file = File::load($target_id);

        if ($file) {
            $variables = [
                'responsive_image_style_id' => $responsive_image_style_id,
                'uri'                       => $file->getFileUri(),
            ];

            $image = $this->imageFactory->get($file->getFileUri());

            if ($image->isValid()) {
                $variables['width'] = $image->getWidth();
                $variables['height'] = $image->getHeight();
            } else {
                $variables['width'] = $variables['height'] = null;
            }

            // Get preload image
            if ($preload_image_style === 'thumbnail' &&
                $preloadImage = $this->imageStyleStorage->load($responsive_image_style_id . '_preload')) {
                $preloadImage = $preloadImage->buildUrl($variables["uri"]);
            } else {
                $preloadImage = $this->getImageStyleUrl($variables["uri"], $preload_image_style);
            }

            template_preprocess_responsive_image($variables);

            if (isset($variables["img_element"])) {
                $imageData = [
                    'preload' => $preloadImage,
                    'image'   => $variables["img_element"]["#uri"],
                    'srcset'  => $variables["img_element"]["#attributes"]["srcset"]->__toString(),
                    'alt'     => isset($variables['img_element']['#alt']) ? $variables['img_element']['#alt'] : '',
                    'title'   => isset($variables['img_element']['#title']) ? $variables['img_element']['#title'] : '',
                ];


                $this->renderer->addCacheableDependency($imageData, $file);

                return $imageData;
            }
        }

        return [];
    }
}
